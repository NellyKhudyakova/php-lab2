<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Худякова Нелли Константиновна 181-322 №</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,500" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<header>
    <div class="header-logo"></div>
    <div class="header-heading"><h1>Худякова Нелли Константиновна 181-322 №</h1></div>
    <div></div>
</header>

<main>
    <?php
    $x = 23;
    $encounting = 5; // максимальное кол-во вычисляемых значений функции
    $step = 2; // шаг фунцкии (на сколько изменяется х при увеличении i на 1)
    $type = 'A'; // тип верстки
    $min_value = -10; // макс допустимое значение функции
    $max_value = 1000; // мин допустимое значение функции
    $precison = 3; // кол-во знаков после запятой при округлении
    // $function_values - массив значений, которые принимает функция $f

    if ((is_numeric($x)) & (is_numeric($encounting)) & (is_numeric($step)) & (is_numeric($min_value)) & (is_numeric($max_value)) & (is_numeric($precison)) & (($type == 'A') || ($type == 'B') || ($type == 'C') || ($type == 'D') || ($type == 'E'))) {
        switch ($type) {
            case 'A':
                for ($i = 0; $i < $encounting; $i++, $x += $step) {
                    if ($x <= 10)
                        $f = 10 * $x - 5;
                    else if (($x < 20) && ($x > 10))
                        $f = ($x + 3) * $x * $x;
                    else if (($x >= 20) && ($x != 25))
                        $f = 3 / ($x - 25) + 2;
                    else if ($x == 25) {
                        echo '<br>' . 'f( 25 ) = error';
                        continue;
                    }

                    $function_values[$i] = $f;

                    if ($i != 0)
                        echo '<br>' . 'f( ' . $x . ' ) = ' . round($f, $precison);
                    else
                        echo 'f( ' . $x . ' ) = ' . round($f, $precison);

                    if (($f < $min_value) || ($f >= $max_value) || ($i == $encounting-1))
                    {
                        echo '<br><hr>'.'<span>Общая сумма значений функции = '.array_sum($function_values).' </span>';
                        echo '<br>'.'<span>Среднее арифметическое значений функции = '. round((array_sum($function_values) / count($function_values)), $precison).' </span>';
                        echo '<br>'.'<span>Минимальное значение функции = '.min($function_values).' </span>';
                        echo '<br>'.'<span>Максимальное значение функции = '.max($function_values).' </span>';
                        break;
                    }
                }

                break;
            case 'B':
                echo '<ul>';
                for ($i = 0; $i < $encounting; $i++, $x += $step) {
                    if ($x <= 10)
                        $f = 10 * $x - 5;
                    else if (($x < 20) && ($x > 10))
                        $f = ($x + 3) * $x * $x;
                    else if (($x >= 20) && ($x != 25))
                        $f = 3 / ($x - 25) + 2;
                    else if ($x == 25) {
                        echo '<li>' . 'f( 25 ) = error' . '</li>';
                        continue;
                    }

                    $function_values[$i] = $f;


                    echo '<li>' . 'f(' . $x . ') = ' . round($f, $precison) . '</li>';

                    if (($f < $min_value) || ($f >= $max_value) || ($i == $encounting-1))
                    {
                        echo '<br><hr>'.'<span>Общая сумма значений функции = '.array_sum($function_values).' </span>';
                        echo '<br>'.'<span>Среднее арифметическое значений функции = '. round((array_sum($function_values) / count($function_values)), $precison).' </span>';
                        echo '<br>'.'<span>Минимальное значение функции = '.min($function_values).' </span>';
                        echo '<br>'.'<span>Максимальное значение функции = '.max($function_values).' </span>';
                        break;
                    }
                }
                echo '</ul>';
                break;
            case 'C':
                echo '<ol>';
                for ($i = 0; $i < $encounting; $i++, $x += $step) {
                    if ($x <= 10)
                        $f = 10 * $x - 5;
                    else if (($x < 20) && ($x > 10))
                        $f = ($x + 3) * $x * $x;
                    else if (($x >= 20) && ($x != 25))
                        $f = 3 / ($x - 25) + 2;
                    else if ($x == 25) {
                        echo '<li>' . 'f( 25 ) = error' . '</li>';
                        continue;
                    }

                    $function_values[$i] = $f;


                    echo '<li>' . 'f(' . $x . ') = ' . round($f, $precison) . '</li>';

                    if (($f < $min_value) || ($f >= $max_value) || ($i == $encounting-1))
                    {
                        echo '<br><hr>'.'<span>Общая сумма значений функции = '.array_sum($function_values).' </span>';
                        echo '<br>'.'<span>Среднее арифметическое значений функции = '. round((array_sum($function_values) / count($function_values)), $precison).' </span>';
                        echo '<br>'.'<span>Минимальное значение функции = '.min($function_values).' </span>';
                        echo '<br>'.'<span>Максимальное значение функции = '.max($function_values).' </span>';
                        break;
                    }
                }
                echo '</ol>';
                break;
            case 'D':
                echo '<table>';
                echo '<tr>';
                echo '<td>№</td>';
                echo '<td> x = </td>';
                echo '<td> f(x) = </td>';
                echo '</tr>';

                for ($i = 0; $i < $encounting; $i++, $x += $step) {
                    if ($x <= 10)
                        $f = 10 * $x - 5;
                    else if (($x < 20) && ($x > 10))
                        $f = ($x + 3) * $x * $x;
                    else if (($x >= 20) && ($x != 25))
                        $f = 3 / ($x - 25) + 2;
                    else if ($x == 25) {
                        echo '<tr>';
                        echo '<td>' . $i . '</td>';
                        echo '<td>' . '25' . '</td>';
                        echo '<td>' . 'error' . '</td>';
                        echo '</tr>';
                        continue;
                    }

                    $function_values[$i] = $f;

                    echo '<tr>';
                    echo '<td>' . $i . '</td>';
                    echo '<td>' . $x . '</td>';
                    echo '<td>' . round($f, $precison) . '</td>';
                    echo '</tr>';

                    if (($f < $min_value) || ($f >= $max_value) || ($i == $encounting-1))
                    {
                        echo '</table>';
                        echo '<br><hr>'.'<span>Общая сумма значений функции = '.array_sum($function_values).' </span>';
                        echo '<br>'.'<span>Среднее арифметическое значений функции = '. round((array_sum($function_values) / count($function_values)), $precison).' </span>';
                        echo '<br>'.'<span>Минимальное значение функции = '.min($function_values).' </span>';
                        echo '<br>'.'<span>Максимальное значение функции = '.max($function_values).' </span>';
                        break;
                    }
                }
                break;
            case 'E':
                for ($i = 0; $i < $encounting; $i++, $x += $step) {
                    if ($x <= 10)
                        $f = 10 * $x - 5;
                    else if (($x < 20) && ($x > 10))
                        $f = ($x + 3) * $x * $x;
                    else if (($x >= 20) && ($x != 25))
                        $f = 3 / ($x - 25) + 2;
                    else if ($x == 25) {
                        echo '<div>' . 'f( 25 ) = error' . '</div>';
                        continue;
                    }

                    $function_values[$i] = $f;

                    echo '<div>' . 'f(' . $x . ') = ' . round($f, $precison) . '</div>';

                    if (($f < $min_value) || ($f >= $max_value) || ($i == $encounting-1))
                    {
                        echo '<br><hr>'.'<span>Общая сумма значений функции = '.array_sum($function_values).' </span>';
                        echo '<br>'.'<span>Среднее арифметическое значений функции = '. round((array_sum($function_values) / count($function_values)), $precison).' </span>';
                        echo '<br>'.'<span>Минимальное значение функции = '.min($function_values).' </span>';
                        echo '<br>'.'<span>Максимальное значение функции = '.max($function_values).' </span>';
                        break;
                    }
                }
                break;
        }
    } else {
        echo '<h1 class="error">Ошибка. Пожалуйста, убедитесь, что всем числовым переменным присвоены числа, а тип верстки A, B, C, D или E</h1>';
    }
    ?>
</main>

<footer>
    <?php
    if (($type == 'A') || ($type == 'B') || ($type == 'C') || ($type == 'D') || ($type == 'E')){
        echo '<div class="type-of-view">'.'Тип верстки " '.$type.' "'.'</div>';
    } else echo '<div class="type-of-view"> Error </div>';

    ?>
</footer>

</body>
</html>